#include <iostream>
#include <cstring>


using namespace std;

class student134
{
    string nume;
    float medie;

public:
    student134();
    student134(string);
    student134(string,float); //initializari(constructori)
    ~student134(); //destructor

    //get
    string get_nume();
    float get_medie();

    //set
    void set_nume(string);
    void set_medie(float);

    void citire();
    void afisare();
};


student134::student134()
{
    medie=0;
    nume="Boris";
}


student134::student134(string s)
{
    nume=s;
    medie=1;
}


student134::student134(string s, float m)
{
    nume=s;
    medie=m;
}


student134::~student134()
{

    nume="Nimic";
    medie=0;

}


string student134::get_nume()
{
    return nume;
}


void student134::set_nume(string s)
{
    nume=s;
}


void student134::set_medie(float m)
{
    medie=m;
}


float student134::get_medie()
{

    return medie;
}

void student134::citire()
{

    cin>>nume>>medie;
}

void student134::afisare()
{
    cout<<nume<<" "<<medie<<endl;
}

class Grupa134
{
    student134 *v;
    int n;

public:
    Grupa134(int);
    void citire();
    void afisare();

};

Grupa134::Grupa134(int x=0)
{
    n=x;
    v=new student134[n];

}

void Grupa134::citire()
{
    cin>>n;
    v=new student134[n];
    for(int i=0; i<n; i++)
        v[i].citire();
}

void Grupa134::afisare()
{
    for(int i=0; i<n; i++)
        v[i].afisare();

}

int main()
{
    Grupa134 razboinicii;
    razboinicii.citire();
    razboinicii.afisare();

    return 0;
}
